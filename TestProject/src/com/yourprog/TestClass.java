package com.yourprog;

import java.util.List;

public class TestClass {

	public static void main(String[] args) {
		var numbers = List.of(2,3,4);
		numbers.forEach(System.out::println);
		
		var strings = List.of("hello","world");
		strings.forEach(System.out::println);
		
		strings
		.stream()
		.filter(e->e.contains("hel"))
		.forEach(System.out::println);
		
		numbers
		.stream()
		.filter(TestClass::isEven)
		.forEach(System.out::println);
	}
	
	public static boolean isEven(Integer number) {
		return number %2 == 0;
	}
}
